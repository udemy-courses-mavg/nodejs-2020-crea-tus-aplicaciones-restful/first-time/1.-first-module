const http = require('http');

/*
const server = http.createServer();

 server.on('connection', (socket)=>{
     console.log("Nueva conexión detectada");
 });

 server.listen(2012);
 console.log("Escuchando en el puerto 2012...");
 */

 /*
 const server = http.createServer((req, res)=>{
     if(req.url === '/'){
         res.write("Hola Mundo ");
         res.write("desde NodeJS");
         res.end();
     }

     if(req.url === '/ejemplo'){
         res.write("Estas en ejemplo");
         res.end();
     }
 });

 server.on('connection', (socket)=>{
     console.log("Nueva conexión detectada");
 });

 const port = 3030;
 server.listen(port);
 console.log("Escuchando en puerto " + port + "...");
*/

const server = http.createServer((req, res)=>{
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<h1> Hola a todos </h1>');
    res.write('<p> Mi web de ejemplo </p>');
    res.end;
}).listen(5050);