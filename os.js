const os = require('os');

console.log('Version SO: ', os.release());
console.log('Memoría Libre: ', os.freemem());
console.log('Memoría Total: ' + os.totalmem());